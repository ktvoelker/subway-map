
.PHONY: all open clean

all: amtrak.png

amtrak.png: amtrak.dot
	dot -Tpng -oamtrak.png amtrak.dot

open: amtrak.png
	open amtrak.png

clean:
	-rm amtrak.png
