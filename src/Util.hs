
module Util where

import Data.Monoid
import Data.String
import Data.Word
import qualified Text.Blaze.Svg11 as Svg

impossible :: a
impossible = error "Impossible!"

baseSizeFactor :: Rational
baseSizeFactor = 1

onFst :: (a -> b) -> (a, c) -> (b, c)
onFst f (x, y) = (f x, y)

onSnd :: (a -> b) -> (c, a) -> (c, b)
onSnd f (x, y) = (x, f y)

showDecimal :: Rational -> Svg.AttributeValue
showDecimal = fromString . show . toDouble

toDouble :: Rational -> Double
toDouble = fromRational

data Color =
  Color
  { cRed   :: Word8
  , cGreen :: Word8
  , cBlue  :: Word8
  } deriving (Eq, Ord, Show)

colorAttr :: Color -> Svg.AttributeValue
colorAttr Color{..} =
  fromString
  $ "rgb(" <> show cRed <> "," <> show cGreen <> "," <> show cBlue <> ")"

white, red, blue, green, black :: Color
white = Color maxBound maxBound maxBound
red   = Color maxBound minBound minBound
green = Color minBound maxBound minBound
blue  = Color minBound minBound maxBound
black = Color minBound minBound minBound

data Dir = N | E | S | W
  deriving (Eq, Ord, Enum, Bounded, Show)

data Pos =
  Pos
  { posX :: Rational
  , posY :: Rational
  } deriving (Eq, Ord, Show)

addPos :: Pos -> Pos -> Pos
addPos p1 p2 = Pos { posX = posX p1 + posX p2, posY = posY p1 + posY p2 }

posTranslation :: Pos -> Svg.AttributeValue
posTranslation Pos{..} =
  "translate(" <> showDecimal posX <> "," <> showDecimal posY <> ")"

