
module Main where

import Prelude hiding (readFile, writeFile, lex)

import Data.ByteString.Lazy (writeFile)
import Data.Functor
import Data.Text.IO (readFile)
import System.Environment (getArgs)
import Text.Blaze.Svg.Renderer.Utf8
import Text.Parsec.Applicative (parse)
import Text.Parsec.Applicative.BNF

import Demo
import Lexer (lex)
import Network
import Page
import Parser (start)

renderToFile :: FilePath -> Drawing Network -> IO ()
renderToFile fileName drawing =
  writeFile fileName
  $ renderSvg
  $ renderDrawing renderNetwork drawing

main :: IO ()
main = getArgs >>= \case
  [inFile, outFile] -> do
    tokens <- lex <$> readFile inFile
    print tokens
    case tokens of
      Left _ -> return ()
      Right ts -> print (parse start ts)
    renderToFile outFile demoDrawing
  _ -> print $ parserToBNF start

