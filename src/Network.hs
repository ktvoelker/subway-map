
module Network
  ( module Network
  , module Station
  ) where

import Debug.Trace

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Monoid
import Data.Text (Text)
import qualified Text.Blaze.Internal as Blaze
import Text.Blaze.Svg11 (Svg, (!))
import qualified Text.Blaze.Svg11 as Svg
import qualified Text.Blaze.Svg11.Attributes as Attr

import Station
import Trig
import Util

data Edge =
  Edge
  { ePort1 :: Port
  , ePort2 :: Port
  , eLabel :: Bool
  } deriving (Eq, Ord, Show)

data Line =
  Line
  { lLabel :: Text
  , lColor :: Color
  , lEdges :: [Edge]
  } deriving (Eq, Ord, Show)

data Network =
  Network
  { nStations :: [Station]
  , nLines    :: [Line]
  } deriving (Eq, Ord, Show)

baseLineWidth :: Rational
baseLineWidth = baseSizeFactor / 4

renderLine :: Map Port Pos -> Line -> Svg
renderLine rPorts Line{..} =
  Svg.g content
  ! Attr.stroke (colorAttr lColor)
  ! Attr.strokeWidth (showDecimal baseLineWidth)
  ! Attr.strokeLinecap "butt"
  where
    content = mconcat $ map (renderEdge rPorts lLabel) lEdges

textpath' :: Svg -> Svg
textpath' = Blaze.customParent "textPath"

renderEdge :: Map Port Pos -> Text -> Edge -> Svg
renderEdge ports label Edge{..} = Svg.g $ do
  Svg.path
    ! Attr.d path
  Svg.g textElem
    ! Attr.transform textTransform
  where
    pos1 = ports Map.! ePort1
    pos2 = ports Map.! ePort2
    segment = mkSegment pos1 pos2
    segMid = segmentMidpoint segment
    segAngle = segmentAngle segment
    perpAngle =
      if segAngle < rightAngle
      then segAngle - rightAngle
      else segAngle + rightAngle
    perpSegment = mkSegmentPolar segMid perpAngle baseLineWidth
    textMid = segB perpSegment
    path =
      Svg.mkPath
      $  Svg.m (toDouble $ posX pos1) (toDouble $ posY pos1)
      >> Svg.l (toDouble $ posX pos2) (toDouble $ posY pos2)
    textElem = Svg.text_ (Svg.toMarkup label)
      ! Attr.fontSize (showDecimal $ baseLineWidth * 3 / 2)
      ! Attr.fill (colorAttr black)
      ! Attr.strokeWidth "0"
      ! Attr.x "0"
      ! Attr.y "0"
      ! Attr.textAnchor "middle"
      ! Attr.dominantBaseline "alphabetic"
    textTransform = posTranslation textMid <> "," <> angleRotation segAngle

renderNetwork :: Network -> Svg
renderNetwork Network{..} = traceShow rPorts content
  where
    (rStations, rPortsPerStation) = unzip $ map renderStation nStations
    rPorts = Map.unions rPortsPerStation
    rLines = map (renderLine rPorts) nLines
    content = mconcat rLines <> mconcat rStations

