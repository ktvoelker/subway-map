
module Parser where

import Data.Text (Text)
import Text.Parsec.Applicative

import Lexer (TT(..), TV(..))
import Station (StationCode(..))
import Util

type P a = Parser Text TT (WithSourcePos TV) a

data StationProp =
    StationLabel Text
  | StationColor Color
  | StationSize Integer Integer
  | StationPosition Pos
  deriving (Eq, Ord, Show)

type Port = (StationCode, PortIndex)

type PortIndex = (Maybe Text, Integer)

data LineProp =
    LineLabel Text
  | LineColor Color
  | LinePath [Port]
  deriving (Eq, Ord, Show)

data Statement =
    SPage Rational Rational
  | SViewport Integer
  | SStation StationCode [StationProp]
  | SLine [LineProp]
  deriving (Eq, Ord, Show)

start :: P [Statement]
start = many statement <* eof

statement :: P Statement
statement = station <|> line <|> page <|> viewport

twoDim :: P a -> P (a, a)
twoDim p = (,) <$> p <* token TCross <*> p

rational :: P Rational
rational = (f . snd <$> token TRational) <|> (fromInteger <$> integer)
  where
    f (WithSourcePos (VRational r) _) = r
    f _ = impossible

integer :: P Integer
integer = f . snd <$> token TInteger
  where
    f (WithSourcePos (VInteger z) _) = z
    f _ = impossible

page :: P Statement
page = fmap (uncurry SPage) $ token TPage *> twoDim (rational <* token TInches)

viewport :: P Statement
viewport = fmap SViewport $ token TViewport *> integer

station :: P Statement
station =
  SStation
  <$> (token TStation *> stationCode <* token TColon)
  <*> (many stationProp <* token TEnd)

line :: P Statement
line = fmap SLine $ token TLine *> many lineProp <* token TEnd

stationProp :: P StationProp
stationProp =
  fmap StationLabel propLabel
  <|> fmap StationColor propColor
  <|> propSize
  <|> propPosition

propSize :: P StationProp
propSize =
  fmap (uncurry StationSize)
  $ token TSize *> token TColon *> twoDim integer

propPosition :: P StationProp
propPosition =
  fmap (StationPosition . uncurry Pos)
  $ token TPosition *> token TColon *> twoDim rational

lineProp :: P LineProp
lineProp =
  fmap LineLabel propLabel
  <|> fmap LineColor propColor
  <|> fmap LinePath (many port)

word :: P Text
word = fmap (f . snd) $ token TWord
  where
    f (WithSourcePos (VText xs) _) = xs
    f _ = impossible

stationCode :: P StationCode
stationCode = StationCode <$> word

port :: P Port
port = (,) <$> stationCode <*> option (Nothing, 0) portNum

portNum :: P (Maybe Text, Integer)
portNum = (,) <$> (token TDot *> optional word) <*> integer

string :: P Text
string = f . snd <$> token TString
  where
    f (WithSourcePos (VText xs) _) = xs
    f _ = impossible

propLabel :: P Text
propLabel = token TLabel *> token TColon *> string

colorValue :: P Color
colorValue = f . snd <$> token TColorValue
  where
    f (WithSourcePos (VColor c) _) = c
    f _ = impossible

propColor :: P Color
propColor = token TColor *> token TColon *> colorValue

