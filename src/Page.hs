
module Page
  ( Page(..)
  , Drawing(..)
  , letterLandscape
  , letterPortrait
  , renderDrawing
  ) where

import Data.Monoid
import Data.Tuple
import Text.Blaze.Svg11 (Svg, (!))
import qualified Text.Blaze.Svg11 as Svg
import qualified Text.Blaze.Svg11.Attributes as Attr

import Util

data Page =
  Page
  { pageXSizeInches :: Rational
  , pageYSizeInches :: Rational
  } deriving (Eq, Ord, Show)

data Drawing a =
  Drawing
  { drawingContent     :: a
  , drawingPageSize    :: Page
  , drawingContentSize :: Integer
  } deriving (Eq, Ord, Show)

letterLandscape :: Page
letterLandscape =
  Page
  { pageXSizeInches = 11
  , pageYSizeInches = 8.5
  }

letterPortrait :: Page
letterPortrait =
  Page
  { pageXSizeInches = 8.5
  , pageYSizeInches = 11
  }

pageAttrs :: Page -> Integer -> Svg -> Svg
pageAttrs page res = pageSize page . pageViewBox page res

pageSize :: Page -> Svg -> Svg
pageSize Page{..} =
  (! Attr.width (showDecimal pageXSizeInches <> "in"))
  . (! Attr.height (showDecimal pageYSizeInches <> "in"))

pageViewBox :: Page -> Integer -> Svg -> Svg
pageViewBox Page{..} res = (! Attr.viewbox attrVal)
  where
    c = calcViewBox res
    ((x0, y0), (dx, dy)) =
      if pageYSizeInches > pageXSizeInches
      then onFst swap $ onSnd swap $ c pageYSizeInches pageXSizeInches
      else c pageXSizeInches pageYSizeInches
    attrVal =
      showDecimal x0
      <> ","
      <> showDecimal y0
      <> ","
      <> showDecimal dx
      <> ","
      <> showDecimal dy

calcViewBox
  :: Integer
  -> Rational
  -> Rational
  -> ((Rational, Rational), (Rational, Rational))
calcViewBox res longInches shortInches = ((x0, y0), (dx, dy))
  where
    xMarginInches = 1
    yMarginInches = 1
    usablePixelsX :: Rational
    usablePixelsX = 1 + fromIntegral res
    pixelsPerInch = usablePixelsX / (longInches - xMarginInches)
    x0 = -1 * xMarginInches * pixelsPerInch
    y0 = -1 * yMarginInches * pixelsPerInch
    dx = longInches * pixelsPerInch
    dy = shortInches * pixelsPerInch

renderDrawing :: (a -> Svg) -> Drawing a -> Svg
renderDrawing f Drawing{..} =
  pageAttrs drawingPageSize drawingContentSize
  $ Svg.docTypeSvg
  $ f drawingContent

