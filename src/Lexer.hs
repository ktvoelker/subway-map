
module Lexer where

import Data.Char
import Data.Functor
import Data.Text (Text, pack, unpack)
import Data.Word (Word8)
import Text.Parsec.Applicative

import Util (Color(..), impossible)

type Lexer a = Parser Text Char SourcePos a

data TT =
    TStation | TEnd | TLine | TPage | TViewport | TInches
  | TLabel | TColor | TSize | TPosition | TPath
  | TColon | TCross | TDot
  | TWord | TInteger | TRational | TString | TColorValue
  deriving (Eq, Ord, Show)

data TV = VText Text | VInteger Integer | VRational Rational | VColor Color
  deriving (Eq, Ord, Show)

instance HasSourcePos TV where
  sourcePos _ = SourcePos Nothing 0 0

type RLexer = Lexer (TT, TV)

pos :: Lexer (tt, td) -> Lexer (tt, WithSourcePos td)
pos parser = f <$> getPosition <*> parser
  where
    f p (tt, td) = (tt, WithSourcePos td p)

keyword :: Text -> TT -> RLexer
keyword xs tt =
  label xs
  . try
  $ sequenceA (map token (unpack xs)) *> pure (tt, VText xs)

keywords :: [(Text, TT)]
keywords =
  [ ("station", TStation)
  , ("end", TEnd)
  , ("line", TLine)
  , ("page", TPage)
  , ("viewport", TViewport)
  , ("in", TInches)
  , ("label", TLabel)
  , ("color", TColor)
  , ("size", TSize)
  , ("pos", TPosition)
  , ("path", TPath)
  , (":", TColon)
  , ("*", TCross)
  , (".", TDot)
  ]

lex :: Text -> Either ParseError [(TT, WithSourcePos TV)]
lex = parse start . stringWithPos . unpack

stringWithPos :: [Char] -> [(Char, SourcePos)]
stringWithPos =
  reverse
  . fst
  . foldl f ([], initialPos Nothing)
  . map (\ch -> (ch, incrPos ch)) 
  where
    f (acc, p) (ch, incr) = ((ch, p) : acc, incr p)

incrPos :: Char -> SourcePos -> SourcePos
incrPos ch (SourcePos n l c)
  | ch == '\n' = SourcePos n (l + 1) 1
  | otherwise  = SourcePos n l (c + 1)

start :: Lexer [(TT, WithSourcePos TV)]
start = label "start" $ many (between ws ws $ pos lexeme) <* eof
  where
    ws = many whitespace *> many (comment *> many whitespace)

lexeme :: RLexer
lexeme = label "lexeme"
  $ try (choice (map (uncurry keyword) keywords))
  <|> string
  <|> number
  <|> color
  <|> word

quote :: Lexer ()
quote = void $ token '"'

oneOf :: [Char] -> Lexer Char
oneOf = fmap fst . choice . map token

printable :: Lexer Char
printable = oneOf $ '\t' : [' ' .. '~']

letter :: Lexer Char
letter = oneOf $ ['A' .. 'Z'] ++ ['a' .. 'z']

digit :: Lexer Char
digit = oneOf ['0' .. '9']

hexDigit :: Lexer Char
hexDigit = oneOf $ ['A' .. 'F'] ++ ['a' .. 'f'] ++ ['0' .. '9']

whitespace :: Lexer Char
whitespace = oneOf [' ', '\t', '\r', '\n']

newline :: Lexer Char
newline = fst <$> token '\n'

string :: RLexer
string =
  label "string"
  . fmap ((TString,) . VText . pack)
  . between quote quote
  $ many printable

comment :: Lexer ()
comment = label "comment" $ void $ between (token '#') newline $ many printable

number :: RLexer
number =
  label "number" $ f <$> some digit <*> optional (token '.' *> some digit)
  where
    f is Nothing = (TInteger, VInteger (read is))
    f is (Just fs) = (TRational, VRational (ip + fp))
      where
        ip = fromInteger (read is)
        fp = fromInteger (read fs) / fromInteger (10 ^ length fs)

word :: RLexer
word =
  label "word"
  . fmap ((TWord,) . VText . pack)
  $ (:) <$> letter <*> many (letter <|> digit <|> fmap fst (token '_'))

color :: RLexer
color =
  label "color"
  . fmap ((TColor,) . VColor . readColor)
  $ token '#' *> sequenceA (replicate 6 hexDigit)

hexByte :: Char -> Char -> Word8
hexByte upper lower = (g upper * 16) + g lower
  where
    g :: Char -> Word8
    g = fromIntegral . f
    f :: Char -> Int
    f c
      | c >= '0' && c <= '9' = ord c - ord '0'
      | c >= 'A' && c <= 'F' = ord c - ord 'A' + 10
      | c >= 'a' && c <= 'f' = ord c - ord 'a' + 10
      | otherwise = impossible

readColor :: [Char] -> Color
readColor [r1, r0, g1, g0, b1, b0] = Color red green blue
  where
    red = hexByte r1 r0
    green = hexByte g1 g0
    blue = hexByte b1 b0
readColor _ = impossible

