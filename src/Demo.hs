
module Demo where

import Network
import Page
import Util

demoDrawing :: Drawing Network
demoDrawing =
  Drawing
  { drawingContent     = demoNetwork
  , drawingPageSize    = letterLandscape
  , drawingContentSize = 20
  }

demoNetwork :: Network
demoNetwork =
  Network
  { nStations = demoStations
  , nLines    = demoLines
  }

demoCHI :: StationCode
demoCHI = StationCode "CHI"

demoMKE :: StationCode
demoMKE = StationCode "MKE"

demoStations :: [Station]
demoStations =
  [ Station
    { sCode  = demoCHI
    , sLabel = ("Chicago", Just (N, 0))
    , sColor = Color minBound minBound maxBound
    , sPortsX = 1
    , sPortsY = 2
    , sPos    = Pos { posX = 14, posY = 12 }
    }
  , Station
    { sCode   = demoMKE
    , sLabel  = ("Milwaukee", Just (E, 0))
    , sColor  = Color maxBound minBound minBound
    , sPortsX = 1
    , sPortsY = 1
    , sPos    = Pos { posX = 10, posY = 4 }
    }
  ]

demoLines :: [Line]
demoLines =
  [ Line
    { lLabel = "Hiawatha"
    , lColor = Color maxBound minBound maxBound
    , lEdges =
      [ Edge
        { ePort1 = (demoCHI, (Nothing, 1))
        , ePort2 = (demoMKE, (Nothing, 0))
        , eLabel = True
        }
      ]
    }
  ]

