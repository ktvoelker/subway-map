
module Station where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Monoid
import Data.Text (Text)
import Text.Blaze.Svg11 (Svg, (!))
import qualified Text.Blaze.Svg11 as Svg
import qualified Text.Blaze.Svg11.Attributes as Attr

import Util

newtype StationCode = StationCode Text
  deriving (Eq, Ord, Show)

data Station =
  Station
  { sCode   :: StationCode
  , sLabel  :: (Text, Maybe (Dir, Integer))
  , sColor  :: Color
  , sPortsX :: Integer
  , sPortsY :: Integer
  , sPos    :: Pos
  } deriving (Eq, Ord, Show)

type PortIndex = (Maybe Dir, Integer)

type Port = (StationCode, PortIndex)

type RenderedStation a = (Svg, Map a Pos)

translateStation :: Pos -> RenderedStation a -> RenderedStation a
translateStation pos (svg, ports) = (svg', ports')
  where
    svg' = Svg.g svg ! Attr.transform (posTranslation pos)
    ports' = Map.map (addPos pos) ports

baseRadius :: Rational
baseRadius = baseSizeFactor / 2

labelStation :: StationCode -> RenderedStation PortIndex -> RenderedStation Port
labelStation code@(StationCode codeText) (svg, ports) = (svg', ports')
  where
    svg' = Svg.g $ svg >> label
    label = Svg.text_ (Svg.toMarkup codeText)
      ! Attr.x "0"
      ! Attr.y "0"
      ! Attr.fontSize (showDecimal $ baseRadius / 2)
      ! Attr.fill (colorAttr white)
      ! Attr.textAnchor "middle"
      ! Attr.dominantBaseline "middle"
    ports' = Map.mapKeys (\index -> (code, index)) ports

renderTinyStation :: RenderedStation PortIndex
renderTinyStation = (svg, ports)
  where
    svg = Svg.circle
      ! Attr.cx "0"
      ! Attr.cy "0"
      ! Attr.r (showDecimal baseRadius)
    ports = Map.fromList
      [ ((Nothing, 0), Pos { posX = 0, posY = 0 })
      ]

renderWideStation :: Integer -> RenderedStation PortIndex
renderWideStation widthInPorts =
  translateStation Pos { posX = -1 * mainWidth / 2, posY = 0 } (svg, ports)
  where
    mainWidth = 2 * baseRadius * fromIntegral widthInPorts
    svg =
      Svg.rect
      ! Attr.x "0"
      ! Attr.y (showDecimal $ -1 * baseRadius)
      ! Attr.width (showDecimal mainWidth)
      ! Attr.height (showDecimal $ 2 * baseRadius)
      ! Attr.rx (showDecimal baseRadius)
      ! Attr.ry (showDecimal baseRadius)
    portNumbers = [0 .. widthInPorts - 1]
    portOffsets = map (\n -> (n, 2 * baseRadius * (0.5 + fromIntegral n))) portNumbers
    narrowPort (n, dx) = ((Nothing, n), Pos { posX = dx, posY = 0 })
    ports = Map.fromList $ map narrowPort portOffsets

rotateStation :: RenderedStation a -> RenderedStation a
rotateStation (svg, ports) = (svg', ports')
  where
    svg' = Svg.g svg ! Attr.transform "rotate(90)"
    ports' = Map.map rotatePos ports

rotatePos :: Pos -> Pos
rotatePos Pos{..} = Pos { posX = negate posY, posY = posX }

renderTallStation :: Integer -> RenderedStation PortIndex
renderTallStation = rotateStation . renderWideStation

renderLargeStation :: Integer -> Integer -> RenderedStation PortIndex
renderLargeStation _ _ = (mempty, Map.empty) -- TODO

styleStation :: Color -> Svg -> Svg
styleStation color content =
  Svg.g content
  ! Attr.fill (colorAttr color)
  ! Attr.strokeWidth "0"

renderStation :: Station -> RenderedStation Port
renderStation Station{..} =
  translateStation sPos
  $ labelStation sCode
  $ onFst (styleStation sColor)
  $ case (sPortsX, sPortsY) of
    (1, 1)   -> renderTinyStation
    (dx, 1)  -> renderWideStation dx
    (1, dy)  -> renderTallStation dy
    (dx, dy) -> renderLargeStation dx dy

