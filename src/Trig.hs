
module Trig where

import Data.Monoid
import qualified Text.Blaze.Svg11 as Svg

import Util

rationalTrig :: (Double -> Double) -> Rational -> Rational
rationalTrig f = toRational . f . fromRational

atan', sin', cos' :: Rational -> Rational
atan' = rationalTrig atan
sin'  = rationalTrig sin
cos'  = rationalTrig cos

pi' :: Rational
pi' = toRational (pi :: Double)

newtype Angle = Angle { radians :: Rational } deriving (Eq, Ord, Show, Num)

degreesPerRadian :: Rational
degreesPerRadian = 360 / (2 * pi')

degrees :: Angle -> Rational
degrees = (* degreesPerRadian) . radians

rightAngle :: Angle
rightAngle = Angle $ pi' / 2

angleRotation :: Angle -> Svg.AttributeValue
angleRotation = ("rotate(" <>) . (<> ")") . showDecimal . degrees

data Segment =
  Segment
  { segA     :: Pos
  , segB     :: Pos
  , segDiffX :: Rational
  , segDiffY :: Rational
  } deriving (Eq, Ord, Show)

mkSegment :: Pos -> Pos -> Segment
mkSegment a b =
  Segment
  { segA = a
  , segB = b
  , segDiffX = posX b - posX a
  , segDiffY = posY b - posY a
  }

segmentAngle :: Segment -> Angle
segmentAngle Segment{..} =
  Angle
  $ if segDiffY == 0
    then 0
    else radians rightAngle - atan' (segDiffX / segDiffY)

segmentMidpoint :: Segment -> Pos
segmentMidpoint Segment{..} =
  Pos
  { posX = posX segA + (segDiffX / 2)
  , posY = posY segA + (segDiffY / 2)
  }

mkSegmentPolar :: Pos -> Angle -> Rational -> Segment
mkSegmentPolar a (Angle theta) dist =
  Segment
  { segA = a
  , segB = b
  , segDiffX = dx
  , segDiffY = dy
  }
  where
    dx = cos' theta * dist
    dy = sin' theta * dist
    b  = Pos { posX = posX a + dx, posY = posY a + dy }

